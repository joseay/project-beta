import React, {useEffect, useState } from "react";

export default function SalesPersonList() {
    const[customers, setCustomers] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
        try {
            const response = await fetch('http://localhost:8090/api/customers/');
            const data = await response.json();
            setCustomers(data.customers);
        } catch (e) {
            console.error('Error: ', e);
        }
    }
    fetchData();
}, []);

    return (
        <table className="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Employee Number</th>
                    <th>Phone Number</th>
                </tr>
            </thead>
            <tbody>
                {customers.map((customer) => { 
                    return (
                        <tr className="table-row" key={customer.id}>
                            <td>{customer.name}</td>
                            <td>{customer.address}</td>
                            <td>{customer.phone_number}</td>
                        </tr>
                        )
                    })}
            </tbody>
        </table>
    );
};

