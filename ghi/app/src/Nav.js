import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="/">
                Home
              </NavLink>
            </li>
            <div className="dropdown">
              <button
                className="btn btn-success dropdown-toggle"
                type="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </button>
              <ul className="dropdown-menu">
              <li className='nav-item'>
                  <NavLink className="dropdown-item" to="employees/salesperson/new">Sales Person Form</NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink className="dropdown-item" to="employees/salesPeople/">Sales Team</NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink className="dropdown-item" to="customers/new"> Customer Form</NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink className="dropdown-item" to="customers/CustomerList"> Customer List </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink className="dropdown-item" to="salesrecord/salesList"> Sales List </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink className="dropdown-item" to="salesrecord/new"> Sales Form </NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button
                className="btn btn-success dropdown-toggle"
                type="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Services
              </button>
              <ul className="dropdown-menu">
              <li className="nav-item">
                  <NavLink className="dropdown-item" to="serviceappointments/list">
                    {" "}
                    Service Appointments List{" "}
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="dropdown-item"
                    to="serviceappointments/new"
                  >
                    {" "}
                    Service Appointments Form{" "}
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="technicians/list">
                    {" "}
                    Technicians List{" "}
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="technicians/new">
                    {" "}
                    Technicians Form{" "}
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="automobiles/list">
                    {" "}
                    Automobiles List{" "}
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="automobiles/new">
                    {" "}
                    Automobiles Form{" "}
                  </NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn btn-success dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
              </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="inventory/manufacturers">Manufacturers</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="inventory/manufacturers/new">New manufacturer</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="inventory/vehicle_models">Vehicle Models</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="inventory/vehicle_models/new">New vehicle model</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="inventory/automobiles">Automobiles</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="inventory/automobiles/new">New automobile</NavLink>
                </li>
              </ul>
            </div>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
