// import { useState, useEffect } from "react";
// import { Link } from "react-router-dom";



// const ServiceAppointmentsList = () => {
//     const [serviceAppointments, setServiceAppointments] = useState([]);

//     UseEffect(() => {
//         const getServiceAppointments = async () => {
//             const url = "http://localhost:8080/api/serviceappointments/";
//             const response = await fetch(url);

//             if (response.ok) {
//                 const data = await response.json();
//                 setServiceAppointments(data.serviceAppointments);
//             }
//         };
//         getServiceAppointments();
//     }, []);

//     const deleteServiceAppointment = async (id) => {
//         const serviceAppointmentUrl = `http://localhost:8080/api/serviceappointments/detail/${id}/`;
//         const response = await fetch(serviceAppointmentUrl, {
//             method: "DELETE",
//         });
//         if (response.ok) {
//             const updatedServiceAppointments = serviceAppointments.filter(
//                 (serviceAppointment) => serviceAppointment.id !== id
//             );
//             setServiceAppointments(updatedServiceAppointments);
//         }
//     };

//     const finishServiceAppointment = async (id) => {
//         const serviceAppointmentUrl = `http://localhost:8080/api/serviceappointments/detail/${id}/`;
//         const response = await fetch(serviceAppointmentUrl, {
//             method: "PATCH",
//         });
//         if (response.ok) {
//             const updatedServiceAppointments = serviceAppointments.filter(
//                 (serviceAppointment) => serviceAppointment.id !== id
//             );
//             setServiceAppointments(updatedServiceAppointments);
//         }
//     };

//     return (
//         <div>
//         <div className="serviceAppointments">
//             <h2>Service Appointments</h2>
//             </div>
//                 <Link to="/serviceappointments/create">
//                     <button className="btn btn-primary">Create Service Appointment</button
//                     >
//                 </Link>
//                 <table className="table table-striped">
//                     <thead>
//                         <tr>
//                             <th>Customer</th>
//                             <th>Vehicle</th>
//                             <th>Service</th>
//                             <th>Technician</th>
//                             <th>Start Date</th>
//                             <th>End Date</th>
//                         </tr>
//                     </thead>
//                     <tbody>
//                         {serviceAppointments.map((serviceAppointment) => {
//                             return (
//                                 <tr key={serviceAppointment.id}>
//                                     <td>{serviceAppointment.customer}</td>
//                                     <td>{serviceAppointment.vehicle}</td>
//                                     <td>{serviceAppointment.service}</td>
//                                     <td>{serviceAppointment.technician}</td>
//                                     <td>{serviceAppointment.start_date}</td>
//                                     <td>{serviceAppointment.end_date}</td>
//                                     <td>
//                                         <button
//                                             className="btn btn-danger"
//                                             onClick={() =>
//                                                 deleteServiceAppointment(serviceAppointment.id)}
//                                                 className="btn btn-danger">
//                                                 cancel
//                                                 </button>
//                                         <button
//                                             className="btn btn-success"
//                                             onClick={() => finishServiceAppointment(serviceAppointment.id)}
//                                             className="btn btn-success">
//                                             finish
//                                         </button>
//                                     </td>
//                                 </tr>
//                             );
//                         })}
//                     </tbody>
//                 </table>
//             </div>
//         );
//     };

//     export default ServiceAppointmentsList;
