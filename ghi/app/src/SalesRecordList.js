import React, { useState, useEffect} from 'react';

export default function SalesList() {
    const [sales, setSales] = useState([]);

    useEffect(() => {
    async function fetchData() {
        const response = await fetch('http://localhost:8090/api/salesrecords/');
        const data = await response.json();
        setSales(data.sales_records);
        console.log(data.sales_records);
    }
    fetchData();
}, []);
return (
    <table className='table table-striped'>
        <thead>
            <tr>
                <td>Sales Person</td>
                <td>Employee Number</td>
                <td>Customer</td>
                <td>Price</td>
                <td>VIN</td>
            </tr>
        </thead>
        <tbody>
            {sales.map((sale) => {
                return(
                <tr key={sale.id}>
                    <td>{sale.sales_person.name}</td>
                    <td>{sale.sales_person.employee_number}</td>
                    <td>{sale.customer.name}</td>
                    <td>{"$" + sale.price}</td>
                    <td>{sale.automobile.import_href.substring(17,sale.automobile.import_href.length-1)}</td>
                </tr>
                );
            })}
        </tbody>
    </table>
    )
}

