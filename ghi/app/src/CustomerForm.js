import React, { useState } from 'react';

export default function PotentialCustomerForm(props) {
    const [name, setName] = useState('')
    const [address, setAddress] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')

    const handleNameInputChange = (event) => {
        setName(event.target.value);
    };

    const handleAddressInputChange = (event) => {
        setAddress(event.target.value);
    };

    const handlePhoneNumberInputChange = (event) => {
        setPhoneNumber(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.address = address;
        data.phone_number = phoneNumber;

        const Url = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: { 'content-type': 'application/json'}
        }


        const response = await fetch(Url, fetchConfig);
        if (response.ok) {
            const newPotentialCustomer = await response.json();
            console.log('hat status', newPotentialCustomer)
            setName('');
            setAddress('');
            setPhoneNumber('');
            props.getCustomer()
        }
    };
    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a potential customer</h1>
                <form onSubmit ={handleSubmit}id="create-customer-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameInputChange}placeholder="name" required type="text" name="name" id="name" className="form-control" value={name}/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleAddressInputChange}placeholder="fabric" required type="text" name="address" id="address" className="form-control" value={address}/>
                        <label htmlFor="address">Address</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlePhoneNumberInputChange}placeholder="Phone Number" required type="digit" name="phoneNumber" id="phone_number" className="form-control" value={phoneNumber}/>
                        <label htmlFor="color">Phone Number</label>
                    </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    </div>
</div>
        )

};
