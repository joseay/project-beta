import React, {useEffect , useState} from 'react';

export default function SalesRecordForm(props) {
    const[automobiles, SetAutomobiles] = useState([]);
    const[salesPerson, setSalesPerson] = useState('');
    const[automobile, SetAutomobile] = useState('');
    const[customer, setCustomers] = useState('');
    const[price, setPrice] = useState('');


    const handleAutomobileInputChange = (event) => {
        SetAutomobile(event.target.value);
    };

    const handleSalesPersonInputChange = (event) => {
        setSalesPerson(event.target.value);
    };

    const handleCustomerInputChange = (event) => {
        setCustomers(event.target.value);      
    };

    const handlePriceInputChange = (event) => {
        setPrice(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.automobile = automobile;
        data.sales_person = salesPerson;
        data.customer = customer;
        data.price = price;

        const url = 'https://localhost:8090/api/salesrecords/'
        const fetchOptions = {
            method:'POST',
            body: JSON.stringify(data),
            headers: {
                'content-type': 'application/json'
            }
        } 

        const salesResponse = await fetch(url, fetchOptions)
        if (salesResponse.ok) {
            SetAutomobile('');
            setSalesPerson('');
            setCustomers('');
            setPrice('');
            props.getSalesPerson();
            props.getCustomer();
            props.getListSales();
            props.getAutomobiles();
        }
    }
    console.log(props.getCustomer)

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new sale</h1>
              <form onSubmit ={handleSubmit} id="create-new-sales-record-form">
              <div className="mb-3">
                <select onChange={handleAutomobileInputChange} required name="automobile" id="automobile" className="form-select" value={automobiles}>
                  <option value="">Choose an automobile</option>
                  {/* {props.automobiles.map(auto =>{
                    return (
                        <option key={auto.id} value={auto.vin}>
                            {auto.vin}
                        </option>
                    )
                })} */}
                </select>
                </div>
                <div className="mb-3">
                <select onChange={handleSalesPersonInputChange} required name="sales_person" id="sales_person" className="form-select" value={salesPerson} >
                  <option value="">Choose a sales person</option>
                  {/* {props.salesPeople.map(salespeople =>{
                    return (
                        <option key={salespeople.id} value={salespeople.id}>
                            {salespeople.name}
                        </option>
                    )
                  })} */}
                </select>
                </div>
                <div className="mb-3">
                <select onChange={handleCustomerInputChange} required name="customer" id="customer" className="form-select" value={customer}>
                  <option value="">Choose a customer</option>
                  {props.customers.map(customer =>{
                    return (
                        <option key={customer.id} value={customer.id}>
                            {customer.name}
                        </option>
                    )
                  })}
                </select>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePriceInputChange}placeholder="price" required type="number" name="price" id="price" className="form-control" value={price} />
                    <label htmlFor="price">Sale price </label>
                </div>
                <button className="btn btn-success btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
)
}


