import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useEffect, useState } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobilesList';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './manufacturerList';
import VehicleModelForm from './VehicleModelForm';
import VehicleModelList from './VehicleModelList';

import SalesPersonForm from './SalesPersonForm';
import SalesRecordForm from './SalesRecordForm';
import SalesPersonList from './SalesPersonList';
import SalesRecordList from './SalesRecordList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import serviceappointmentslist from './ServiceAppointmentsList';
import serviceappointmentsform from './ServiceAppointmentsForm';
import technicianslist from './TechniciansList';
import techniciansform from './TechniciansForm';



export default function App() {

  const [manufacturers, setManufacturers] = useState([])
  const [vehicleModels, setVehicleModels] = useState([])
  const [automobiles, setAutomobiles] = useState([])

  const [customers, setCustomers] = useState([]);
  const [salesPeople, setSalesPeople] = useState([]);
  const [listSales, setListSales] = useState([]);
  const [serviceAppointments, setServiceAppointments] = useState([]);
  const [technicians, setTechnicians] = useState([]);


  const getVehicleModels = async () => {
    const url = "http://localhost:8100/api/models/"
    const response = await fetch (url)
    if (response.ok){
        const data = await response.json();
        const vehicleModels = data.models
        setVehicleModels(vehicleModels)
    }
  }

  const getManufacturers = async () => {
    const url = "http://localhost:8100/api/manufacturers/"
    const response = await fetch (url)
    if (response.ok){
        const data = await response.json();
        const manufacturers = data.manufacturers
        setManufacturers(manufacturers)
    }
  }

  const getAutomobiles = async () => {
    const url = "http://localhost:8100/api/automobiles"
    const response = await fetch (url)
    if (response.ok){
        const data = await response.json();
        const automobiles = data.autos
        setAutomobiles(automobiles)
    }
  }

  const getListSales = async() => {
    const url = 'http://localhost:8090/api/salesrecords';
    const response = await fetch (url);
    if (response.ok) {
      const data = await response.json();
      setListSales(data.sales_record);
    }
  };

  const getSalesPerson = async() => {
    const url = 'http://localhost:8090/api/salespeople/'
    const reponse = await fetch(url);
    if (reponse.ok) {
      const data  = await reponse.json();
      const sales_people = data.salesPeople;
      setSalesPeople(sales_people);
    }
  };

  const getCustomers = async ()=> {
    const url = 'http://localhost:8090/api/customers/'
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json()
      const customers = data.customers
      setCustomers(customers)
    }
  };



const getServiceAppointments = async () => {
  const url = 'http://localhost:8090/api/serviceappointments/';
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    setServiceAppointments(data)
    }
  };

const getTechnicians = async () => {
  const url = 'http://localhost:8090/api/technicians/';
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    setTechnicians(data)
    }
};



  useEffect(() => {
    getAutomobiles();
    getVehicleModels();
    getManufacturers();
    getSalesPerson();
    getCustomers();
    getListSales();
    getServiceAppointments();
    getTechnicians();
  }, []);


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
        <Route path="inventory">
            <Route path="manufacturers">
              <Route index element={<ManufacturerList manufacturers={manufacturers} />} />
              <Route path="new" element={<ManufacturerForm getManufacturers={getManufacturers} />} />
            </Route>
            <Route path="vehicle_models">
              <Route index element={<VehicleModelList vehicleModels={vehicleModels} />} />
              <Route path="new" element={<VehicleModelForm manufacturers={manufacturers} getVehicleModels={getVehicleModels} />} />
            </Route>
            <Route path="automobiles">
              <Route index element={<AutomobileList automobiles={automobiles} />}/>
              <Route path="new" element={<AutomobileForm vehicleModels={vehicleModels} getAutomobiles={getAutomobiles} />} />
            </Route>
          </Route>
          <Route path="/" element={<MainPage/>} />
          <Route path='salesrecord'>
            <Route path="new/" element={<SalesRecordForm customers={customers} SalesPeople={salesPeople} getSalesPerson={getSalesPerson} getCustomers={getCustomers} getListSales={getListSales}/>} />
            <Route path="salesList/" element={<SalesRecordList />} />
          </Route>
          <Route path='employees'>
            <Route path="salesperson/new" element={<SalesPersonForm getSalesPerson={getSalesPerson} />} />
            <Route path='salesPeople/' element={<SalesPersonList/>} />
          </Route>
          <Route path='customers'>
            <Route path="new/" element={<CustomerForm getCustomers={getCustomers} />} />
            <Route path="CustomerList/" element={<CustomerList/>} />
          </Route>
          <Route path='salesrecord'>
            <Route path="new/" element={<SalesRecordForm customers={customers} SalesPeople={salesPeople} getSalesPerson={getSalesPerson} getCustomers={getCustomers} getListSales={getListSales}/>} />
            <Route path="salesList/" element={<SalesRecordList />} />
          </Route>
          <Route path='serviceappointments'>
            <Route path="list/" element={<serviceappointmentslist />} />
            <Route path="new/" element={<serviceappointmentsform />} />
          </Route>
          <Route path='technicians'>
            <Route path="list/" element={<technicianslist />} />
            <Route path="new/" element={<techniciansform />} />
          </Route>
          <Route path='automobiles'>
            <Route path="list/" element={<automobileslist />} />
            <Route path="new/" element={<automobilesform />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
  }
