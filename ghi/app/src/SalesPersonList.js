import React, {useEffect, useState } from "react";

export default function SalesPersonList() {
    const[salesPeople, SetSalesPeople] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
        try {
            const response = await fetch('http://localhost:8090/api/salespeople/');
            const data = await response.json();
            SetSalesPeople(data.salesPeople);
        } catch (e) {
            console.error('Error: ', e);
        }
    }
    fetchData();
}, []);

    return (
        <table className="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Employee Number</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {salesPeople.map(salesPerson => { 
                    return (
                        <tr className="table-row" key={salesPerson.id}>
                            <td>{salesPerson.name}</td>
                            <td>{salesPerson.employee_number}</td>
                        </tr>
                        )
                    })}
            </tbody>
        </table>
    );
};

