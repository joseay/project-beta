// import React, { useState } from "react";



// const ServiceAppointmentsForm = (props) => {
//     const [vin, setVin] = useState("");
//     const [owner, setOwner] = useState("");
//     const [dateTime, setDateTime] = useState("");
//     const [technicians, setTechnicians] = useState([]);
//     const [selectedTechnician, setSelectedTechnician] = useState("");
//     const [reason, setReason] = useState([]);
//     const [submitted, setSubmitted] = useState(false);

//     useEffect(() => {
//         const getTechnicians = async () => {
//             const url = "http://localhost:8080/api/technicians/";
//             const response = await fetch(url);

//             if (response.ok) {
//                 const data = await response.json();
//                 setTechnicians(data.technicians);
//             }
//         };
//         getTechnicians();
//     }, []);

//     const clearState = () => {
//         setVin("");
//         setOwner("");
//         setDateTime("");
//         setSelectedTechnician("");
//         setReason("");
//         setSubmitted(true);
//     };

//     const handleSubmit = async (event) => {
//         event.preventDefault();

//         const data = {};
//         data.vin = vin;
//         data.owner = owner;
//         data.date_time = dateTime;
//         data.technician_id = selectedTechnician;
//         data.reason = reason;

//         const url = "http://localhost:8080/api/serviceappointments/";
//         const response = await fetch(url, {
//             method: "post",
//             body: JSON.stringify(data),
//             headers: {
//                 "Content-Type": "application/json",
//             },
//         });

//         if (response.ok) {
//             event.target.reset();
//             clearState();
//         }
//     };

//     return (
//         <div className="row">
//             <div className="offset-3 col-6">
//                 <div className="shadow p-4 mt-4">
//                     <h1 className="text-center">New Service Appointment</h1>
//                     <form id="create-appointment-form" onSubmit={handleSubmit}>
//                         <div className="form-group">
//                             <label htmlFor="vin">VIN</label>
//                             <input
//                                 type="text"
//                                 className="form-control"
//                                 id="vin"
//                                 name="vin"
//                                 onChange={(event) => setVin(event.target.value)}
//                             />
//                         </div>
//                         <div className="form-group">
//                             <label htmlFor="owner">Owner</label>
//                             <input
//                                 type="text"
//                                 className="form-control"
//                                 id="owner"
//                                 name="owner"
//                                 onChange={(event) => setOwner(event.target.value)}
//                             />
//                         </div>
//                         <div className="form-group">
//                             <label htmlFor="date_time">Date and Time</label>
//                             <input
//                                 type="datetime-local"
//                                 className="form-control"
//                                 id="date_time"
//                                 name="date_time"
//                                 onChange={(event) =>
//                                     setDateTime(event.target.value)
//                                 }
//                             />
//                         </div>
//                         <div className="form-group">
//                             <label htmlFor="technician">Technician</label>
//                             <select
//                                 className="form-control"
//                                 id="technician"
//                                 name="technician"
//                                 onChange={(event) =>
//                                     setSelectedTechnician(event.target.value)
//                                 }
//                             >
//                                 <option value="">Select a technician</option>
//                                 {technicians.map((technician) => (
//                                     <option
//                                         key={technician.id}
//                                         value={technician.id}
//                                     >
//                                         {technician.name}
//                                     </option>
//                                 ))}
//                             </select>
//                         </div>
//                         <div className="form-group">
//                             <label htmlFor="reason">Reason</label>
//                             <textarea
//                                 className="form-control"
//                                 id="reason"
//                                 name="reason"
//                                 onChange={(event) => setReason(event.target.value)}
//                             />
//                         </div>
//                         <button type="submit" className="btn btn-primary">
//                             Submit
//                         </button>
//                     </form>
//                 </div>
//             </div>
//         </div>
//     );
// };

// export default ServiceAppointmentsForm;
