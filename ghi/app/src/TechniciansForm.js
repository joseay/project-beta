import React, { useState } from "react";

const TechnicianForm = () => {
    const [name, setName] = useState("");
    const [employeeNumber, setEmployeeNumber] = useState("");
    const [submitted, setSubmitted] = useState(false);
    const [invalid, setInvalid] = useState(false);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const employee_number = employeeNumber;
        const data = { name, employee_number };

        const techUrl = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(techUrl, fetchConfig);

        if (response.ok) {
            event.target.reset();
            setName("");
            setEmployeeNumber("");
            setSubmitted(true);
        } else {
            setInvalid(true);
        }
    };

    return (
        <div className="row">
            <div className="col-md-12">
                <h2 className="text-center">Add Technician</h2>
            <form id="create-technician-form" onSubmit={handleSubmit}>
                <div className="form-floating mb-3">
                    <input
                        onChange={(e) => setName(e.target.value)}
                        placeholder="Name"
                        required
                        type="text"
                        name="name"
                        id="name"
                        className="form-control"
                    />
                    <label htmlFor="name">Employee Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input
                        onChange={(e) => setEmployeeNumber(e.target.value)}
                        placeholder="Employee Number"
                        required
                        type="text"
                        name="employeeNumber"
                        id="employeeNumber"
                        className="form-control"
                    />
                    <label htmlFor="employeeNumber">Employee Number</label>
                </div>
                <div className="d-grid">
                    <button type="submit" className="btn btn-primary">
                        Create Technician
                    </button>
                </div>
            </form>
            <div
                className="alert alert-success mt-3"
                id="success-alert"
                role="alert"
                style={{ display: submitted ? "block" : "none" }}
            >
                Technician created successfully! you did it, congratulations!
            </div>
            <div
                className="alert alert-danger mt-3"
                id="invalid-alert"
                role="alert"
                style={{ display: invalid ? "block" : "none" }}
            >
                Invalid employee number, please try again, or don't it's up to you really.
            </div>
            </div>
        </div>
    );
};

export default TechnicianForm;



