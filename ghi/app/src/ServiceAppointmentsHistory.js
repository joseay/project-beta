import React, { useState, useEffect } from "react";


const ServiceAppointmentsHistory = () => {
    const [serviceAppointments, setServiceAppointments] = useState([]);
    const [filtered, setFiltered] = useState([]);
    const [searchVin, setSearchVin] = useState("");
    const [submitted, setSubmitted] = useState(false);


    useEffect(() => {
        const getServiceAppointments = async () => {
            const url = "http://localhost:8080/api/serviceappointments/";
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setServiceAppointments(data.serviceAppointments);
            }
        };
        getServiceAppointments();
    }, []);

    const handleSearch = async (event) => {
        const results = serviceAppointments.filter((serviceAppointment) =>
            serviceAppointment.vin.includes(searchVin)
        );
        setFiltered(results);
        setSubmitted(true);
    };

    const handleReset = () => {
        setSearchVin("");
        setSubmitted(false);
    };

    return (
        <React.Fragment>
            <div className="px-4 py-5 my-1 mt-0 text-center">
                <h1 className="display-5">Service Appointment History</h1>
            </div>

