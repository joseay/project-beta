# CarCar

Team:

* Person 1 Jose- Which microservice? sales
* Person 2 Kelvin- Which microservice? service

# Get Started 
1. make sure you have Docker installed on your computer.
2. create a volume called "beta-data"
3. using the command "docker volume create beta-data".
4. build the containers using "docker-compose build"
5. Start containers using command "docker-compose up". Once the containers are running, 
6. access the application at "http://localhost:3000/".

# API Documentation
```
PORT 3000 - Connect to React APP
Port 15432 - Postgres DB
Port 8080 - Service API
Port 8090 - Sales API
Port 8100 - Inventory API

Salespeople API
- Method: GET
URL: http://localhost:8090/api/salespeople/
- Method: POST
URL: http://localhost:8090/api/salespeople/
- Method: GET
URL: http://localhost:8090/api/salesrecords/
- Method: POST
URL: http://localhost:8090/api/salesrecords/


Customers API
- Method: GET
URL: http://localhost:8090/api/customers/
- Method: POST
URL: http://localhost:8090/api/customers/


get method List service appointments:
- localhost:8080/api/serviceappointments/
get method show service appointments (form):
- localhost:8080/api/serviceappointments/2/ 

get method list technicians:
- localhost:8080/api/technicians/
post method create technician:
- localhost:8080/api/technicians/

```

## Design



## Service microservice
- This service is an application called "Automobile Service" that lets users schedule and keep track of service appointments for cars and their owners. On the application's front end, there are forms for making and managing service appointments, as well as a list of appointments that have already been set. The back-end code makes it possible to create automotive technicians and service appointments, as well as store and get information about service appointments. There are links in the navigation bar that take you to forms where you can enter a technician's name and employee number, as well as a service appointment. The app also has a list of scheduled appointments, a page that shows the service history for a specific VIN, and a button that a service concierge can use to cancel or finish an appointment. The service also connects to an Inventory microservice at a point where it checks to see if the VIN of a vehicle is for a car that was in the inventory at one point, which would show that the car was bought from the dealership.

## Sales microservice
- Back-End is made up of two main directories: "api" and "poll". The "api" directory includes three subdirectories: "common", "sales_project", and "sales_rest". Inside "common", there is a "json.py" file that contains three encoders for ease of use when creating models and views. The "sales_project" directory includes important files such as "settings" and "urls", where apps and middleware are installed and permission is given to certain ports, and admin and API URLs are placed, respectively. The "sales_rest" directory includes important files such as "admin", "api_urls", "api_views", "apps" and "models". In "admin", the "AutomobileVO" is registered, in "api_urls" microservices' URLs are placed, in "api_views" encoders and view functions are placed, in "apps" the classname is put into the "settings.py" file and in "models" the view functions' models are placed. The "poll" directory includes a "poller" file that contains functions to poll and retrieve data.

- For the Front-End you will need to check the ghi.app.src directory. It contains three main components: "App", "MainPage", and "Nav". The "App" component sets the routes for each microservice, the "MainPage" component displays the main page of the service browser, and the "Nav" component sets the navigation bar on the web browser. Additionally, there are several form components such as "AutoForm", "CustomerForm", "ManufactuerForm", "ModelForm", "SalesPersonForm", and "SalesRecordForm" for creating new records. There are also "AutoList", "CustomerList", "ManufacturerList", "ModelList", "SalesPersonList", and "SalesRecordList" components that display lists of records.

- The sales poller generates the "AutomobileVO" which comprises the attributes 'color', 'year', 'VIN', 'model', and 'import_href'. This can be employed to poll data from the Inventory and generate new entries utilizing the microservices.
