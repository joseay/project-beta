from .models import SalesPerson, SalesRecord, Customer, AutomobileVO
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
from django.http import Http404
from django.shortcuts import get_object_or_404
import json

# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties =[
        'id',
        'sold',
        'import_href',
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        'id',
        'name',
        'employee_number'
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        'id',
        'name',
        'address',
        'phone_number',
        ]

class SalesRecorderEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        'id',
        'automobile',
        'sales_person',
        'customer',
        'price',
        ]
    encoders = {
        'automobile': AutomobileVOEncoder(),
        'sales_person': SalesPersonEncoder(),
        'customer': CustomerEncoder(),
    }


@require_http_methods(['GET', 'POST'])
def  api_sales_poeple_list(request):
    if request.method == 'GET':
        sales_people = SalesPerson.objects.all()
        return JsonResponse({'salesPeople': sales_people,},encoder=SalesPersonEncoder)
    else:
        try:
            content = json.loads(request.body)
            sales_people = SalesPerson.objects.create(**content)
            return JsonResponse(sales_people,encoder=SalesPersonEncoder, safe=False)
        except:
            return JsonResponse(
                {'message': 'Could not create the sales person.'}, status=400)


@require_http_methods(['GET', 'POST'])
def api_customer_list(request):
    if request.method == 'GET':
        customers = Customer.objects.all()
        return JsonResponse({'customers': customers}, encoder = CustomerEncoder)
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False)
        except:
            return JsonResponse({'message': 'Could not create a potential customer'}, status=400)


@require_http_methods(['GET', 'POST'])
def api_sales_record_list(request,):
    if request.method=='GET':
            sales_records = SalesRecord.objects.all()
            return JsonResponse({'sales_records': sales_records}, encoder=SalesRecorderEncoder)
    else:
            content = json.loads(request.body)
            content['customer'] = Customer.objects.get(phone_number=content['customer'])
            content['sales_person'] = SalesPerson.objects.get(employee_number=content['sales_person'])
            try:
                content['automobile'] = AutomobileVO.objects.get(vin=content['automobile'])
            except AutomobileVO.DoesNotExist:
                return JsonResponse({'message': 'Automobile does not exist'}, sales_records, encoder=SalesRecorderEncoder,safe=False)

@require_http_methods(['GET'])
def api_sales_record_detail(request, id):
    if request.method == 'GET':
        try:
            sales_recods = SalesRecord.objects.filter(sales_person=id)
            return JsonResponse(
                sales_recods,
                encoder=SalesRecorderEncoder,
                safe=False
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse({'message': 'Does not exist'},status=404)
