from django import urls
from django.urls import path
from .views import (
    api_sales_record_list,
    api_sales_poeple_list,
    api_customer_list,
)

urlpatterns = [
    path('salespeople/', api_sales_poeple_list, name='sales_poeple_list'),
    path('customers/', api_customer_list, name='customer_list'),
    path('salesrecords/', api_sales_record_list, name='sales_record_list'),
    path('salespeople/<int:id>/salesrecords', api_sales_poeple_list, name='sales_poeple_list'),
]

