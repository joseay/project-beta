from django.db import models
# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=20, unique=True)
    sold = models.BooleanField(default=False)
    import_href = models.CharField(max_length=200, unique=True, null=True)

    def __str__(self):
        return self.vin
    
class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=250)
    phone_number = models.CharField(max_length=20, null=True)

    def __str__(self):
        return self.name

class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.name

class SalesRecord(models.Model):
    sales_person = models.ForeignKey(SalesPerson, related_name= 'auto_sales', on_delete=models.PROTECT,null=True)
    automobile = models.ForeignKey(AutomobileVO, related_name= 'auto_sales', on_delete=models.PROTECT)
    customer = models.ForeignKey(Customer, related_name= 'auto_sales', on_delete=models.PROTECT)
    price = models.PositiveIntegerField()
    
    def __str__(self):
        return self.sales_person.employee_number
