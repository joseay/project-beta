from django.shortcuts import render
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import ServiceAppointments, Technician, AutomobileVO


class TechnicianEncoder(ModelEncoder):
  model = Technician
  properties = [
    'id',
    'technician_name',
    'employee_number',
  ]


class AutomobileVOEncoder(ModelEncoder):
  model = AutomobileVO
  properties = [
    "import_href",
    'vin',
    'color',
    'year',
    'model_name',
  ]


class ServiceAppointmentsEncoder(ModelEncoder):
  model = ServiceAppointments
  properties = [
    'id',
    'customer',
    'date_time',
    'reason',
    'vip',
    'technician_name',
    'vin',
    # 'import_href',
  ]
  encoders = {
  "technician_name": TechnicianEncoder(),
  "vin": AutomobileVOEncoder(),
  }

  class HistoryEncoder(ModelEncoder):
    model = ServiceAppointments
    properties = [
      'id',
      'customer',
      'date_time',
      'reason',
      'vip',
      'technician_name',
      'vin',
      # 'import_href',
    ]
    encoders = {
      "technician_name": TechnicianEncoder(),
      "vin": AutomobileVOEncoder(),
    }




class ServiceAppointmentsDetailEncoder(ModelEncoder):
  model = ServiceAppointments
  properties = [
    'customer',
    'date_time',
    'reason',
    'vip',
    'technician_name',
    'vin',
    # 'import_href',
  ]
  encoders = {
    "technician_name": TechnicianEncoder(),
    "vin": AutomobileVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_service_appointments(request):
    if request.method == "GET":
        appointments = ServiceAppointments.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=ServiceAppointmentsEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            service_technician = Technician.objects.get(id=content["technician_name"])
            content["technician_name"] = service_technician

            # try:
            #     vipVIN = AutomobileVO.objects.get(vin=content['vin'])
            #     if vipVIN:
            #         content["vip"] = True
            # except AutomobileVO.DoesNotExist:
            #     content["vip"] = False
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid service technician id"},
                status=400,
            )
        appointment = ServiceAppointments.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=ServiceAppointmentsEncoder,
            safe=False,
        )



@require_http_methods(["GET", "PUT"])
def api_show_service_appointment(request, pk):
    if request.method == "GET":
        service_appointment = ServiceAppointments.objects.get(id=pk)
        return JsonResponse(
            service_appointment,
            encoder=ServiceAppointmentsDetailEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        print(content, "LINE 110______________")
        if "technician_name" in content:
            technician = Technician.objects.get(id=content["technician_name"])
            content["technician_name"] = technician
        ServiceAppointments.objects.filter(id=pk).update(**content)
        service_appointment = ServiceAppointments.objects.get(id=pk)
        return JsonResponse(
            service_appointment,
            encoder=ServiceAppointmentsDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET"])
def service_appointments_history(request):
    if request.method == "GET":
        service_appointments_history = ServiceAppointments.objects.filter(vip=True)
        if service_appointments_history == None:
            return JsonResponse(
                {"message": "No VIP appointments"},
                status=400,
                safe=False,
            )
        else:
            print("here is the history", service_appointments_history)
            return JsonResponse(
                {"service_appointments_history": service_appointments_history},
                encoder=HistoryEncoder,
                safe=False,
            )


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
         technicians = Technician.objects.all()
         return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False,
            )
    else:
        try:
            content = json.loads(request.body)
            technicians = Technician.objects.create(**content)
            return JsonResponse(
                    technicians,
                    encoder=TechnicianEncoder,
                    safe=False,
                )
        except:
            response = JsonResponse(
                {"message": "Technician Id exists already"}
            )
            response.status_code = 500
            return response



@require_http_methods(["GET", "POST"])
def api_list_automobiles(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            AutomobileVOEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        automobile = AutomobileVO.objects.create(**content)
        return JsonResponse(
            automobile,
            encoder=AutomobileVOEncoder,
            safe=False,
        )

