from django.contrib import admin
from .models import ServiceAppointments, Technician, AutomobileVO

# Register your models here.
admin.site.register(ServiceAppointments)
admin.site.register(Technician)
admin.site.register(AutomobileVO)
