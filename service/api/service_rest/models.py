from django.db import models
from django.urls import reverse



class ServiceAppointments(models.Model):
    customer = models.CharField(max_length=100)
    date_time = models.DateTimeField(null=True, blank=True)
    reason = models.CharField(max_length=300)
    vip = models.BooleanField(default=False)
    technician_name = models.ForeignKey(
        'Technician',
        on_delete=models.PROTECT,
    )
    vin = models.ForeignKey(
        'AutomobileVO',
        related_name="services",
        on_delete=models.PROTECT
    )

    def __str__(self):
        return self.customer


class Technician(models.Model):
    technician_name = models.CharField(max_length=100)
    employee_number = models.PositiveBigIntegerField()

    def __str__(self):
        return self.technician_name

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=100, null=True, blank=True, unique=True)
    vin = models.CharField(max_length=20, unique=True)
    color = models.CharField(max_length=20)
    year = models.PositiveSmallIntegerField()
    model_name = models.CharField(max_length=100)
