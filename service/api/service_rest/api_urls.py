from django.urls import path
from .api_views import (
    api_list_service_appointments,
    api_show_service_appointment,
    api_list_technicians,
    api_list_automobiles,
)

urlpatterns = [
    path("serviceappointments/", api_list_service_appointments, name="api_list_service_appointments"),
    path( "serviceappointments/<int:pk>/", api_show_service_appointment,name="api_show_service_appointment"),
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("automobiles/", api_list_automobiles, name="api_list_automobiles"),
]
